const { gql } = require("apollo-server");

module.exports = gql`
  #-----obj types-----#
  type Post {
    id: ID!
    body: String!
    createdAt: String!
    username: String!
  }

  type User {
    id: ID!
    email: String!
    token: String!
    username: String!
    createdAt: String!
  }
  input RegisterInput {
    username: String!
    password: String!
    confirmPassword: String!
    email: String!
  }

  #-----root types-----#
  type Query {
    #query fields
    sayHi: String!
    getPosts: [Post]
  }

  type Mutation {
    #mtuation fields
    register(registerInput: RegisterInput): User!
    login(username: String!, password: String!): User!
  }
`;
