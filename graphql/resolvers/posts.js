const Post = require("../../models/Post");
module.exports = {
  Query: {
    async getPosts() {
      try {
        const allPosts = await Post.find();
        return allPosts;
      } catch (error) {
        console.log("getPosts request failed!");
        throw new Error(error);
      }
    },
  },
};
