const userResolve = require("./users");
const postResolve = require("./posts");

module.exports = {
  Query: {
    ...userResolve.Query,
    ...postResolve.Query,
  },
  Mutation: {
    ...userResolve.Mutation,
  },
};
